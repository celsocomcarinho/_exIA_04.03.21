using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PositionBillboard : MonoBehaviour
{
    public Text txt_X;
    public Text txt_Y;
    public Text txt_Z;

    void Update()
    {
        float x = Mathf.Round(transform.position.x);
        float y = Mathf.Round(transform.position.y);
        float z = Mathf.Round(transform.position.z);

        txt_X.text = x.ToString();
        txt_Y.text = y.ToString();
        txt_Z.text = z.ToString();
    }
}
