using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class MoveToTarget : MonoBehaviour
{
    [Range(0.001f, 5.0f)]
    public float speed = 0.1f;
    [Range(0.1f, 10.0f)]
    public float stopRange = 1f;
    public bool go = true;
    public bool moving = false;
    public bool keepMove = false;
    public Transform target;
    public List<Transform> targetsList;
    public int index = 0;

    void Update()
    {
        if (target != null && !moving && go)
            StartCoroutine(GoForIt());
    }

    IEnumerator GoForIt()
    {
        //go aqui é só um trigger
        go = false;
        moving = true;
        float startDistance = GetDistance();
        float startTime = Time.time;

        while (GetDistance() > stopRange && moving)
        {
            float distCovered = (Time.time - startTime) * speed;
            float step = distCovered / startDistance;
            transform.position = Vector3.Lerp(transform.position, target.position, step);
            
            //distância de parada < 0.05, considera posição exata do target
            if(stopRange <= 0.05f && GetDistance() <= 0.05f)
            {
                moving = false;
                transform.position = target.position;
            } 

            yield return null;
        }

        //chegou
        moving = false;

        //keepMove marcado, inicia novo trajeto
        if (keepMove)
        {
            //lista vazia, mesmo target
            go = true;

            //algo na lista, troca a index
            if (targetsList != null)
            {
                index++;
                if (index > targetsList.Count)
                    index = 0;
                target = targetsList[index];
            }
        }
        else
        {
            //keepMove desmarcado, perde alvo
            target = null;
        }

        yield break;
    }
    float GetDistance()
    {
        //retorna a distância atual do target atual
        return Vector3.Distance(target.position, transform.position);
    }
}
